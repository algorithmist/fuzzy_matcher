﻿import re, math
from collections import defaultdict, Counter
from operator import itemgetter
from time import time

try:
    from .utils import Metaphone, jaccard, character_ngrams, get_char_values, get_numbers, to_float
    from .utils import CHAR_IMPORTANCE
except:
    from utils import Metaphone, jaccard, character_ngrams, get_char_values, get_numbers, to_float
    from utils import CHAR_IMPORTANCE

try:
    from .cleaning import clean_text, remove_all_punctuation
except:
    from cleaning import clean_text, remove_all_punctuation

try:
    from .edit_distance import distance, edits
except:
    from edit_distance import distance, edits
    
__all__ = ["FuzzyTexts"]

cdef dict e_CHAR_IMPORTANCE = <dict>CHAR_IMPORTANCE
cdef int i_MAX_CHAR_VAL = max(e_CHAR_IMPORTANCE.values())
#print('i_MAX_CHAR_VAL', i_MAX_CHAR_VAL)

o_METAPHONE = Metaphone()

re_PUNCT = re.compile(r"[^A-Za-z0-9]")

cdef class FuzzyTexts:
    ''' Fuzzy matching on shared character ngrams
        This is primarily aimed at medium to long strings like titles, brands.
        Not ideal for being spellchecker with single words tokens
    '''
    cdef public int _min_ngram_chars, _max_ngram_chars, _min_matches, _max_matches, _max_start_len, _max_end_len
    cdef public bint _already_clean, _phonetic_index, _ngrams_cross_boundaries
    cdef public float _length_weight, _edit_weight
    cdef public float _ngram_weight, _phonetic_weight
    cdef public float _start_weight, _end_weight, _number_weight
    cdef public float _contains_search_weight, _contains_match_weight, _important_word_factor
    cdef public dict _ngram_len_scores, cache
    cdef public ngrams
    cdef public list texts, bare, phon
    cdef bint _cache_searches
    cdef public set _unimportant_words, _less_important_words
    
    def __repr__(self):
        s_precleaned = " pre-cleaned " if self._already_clean else " "
        s_repr = f"FuzzyTexts: {len(self.texts):,}{s_precleaned}texts indexed on ngrams of {self._min_ngram_chars} to {self._max_ngram_chars} characters."
        if self._ngrams_cross_boundaries:
            s_repr += " Will index char-ngrams that span word boundaries."
        
        i_weight_prec = 5
        s_repr += f"""
Scoring similarity of two texts:
\tSame character length: {round(self._length_weight, i_weight_prec)}
\t# Levenshtein edits: {round(self._edit_weight, i_weight_prec)}
\t# shared character ngrams: {round(self._ngram_weight, i_weight_prec)}
\tSounding the same: {round(self._phonetic_weight, i_weight_prec)}
\tStarting with same text: {round(self._start_weight, i_weight_prec)}
\tEnding with same text: {round(self._end_weight, i_weight_prec)}
\tExact matches on numbers in text: {round(self._number_weight, i_weight_prec)}
\tMatch containing all of search's words: {round(self._contains_search_weight, i_weight_prec)}
\tSearch containing all of match's words: {round(self._contains_match_weight, i_weight_prec)}
"""
        return s_repr
    
    def __init__(
        self,
        texts,  # list of texts to compare to
        i_min_ngram_chars=5,  # minimum character length before comparing
        i_max_ngram_chars=25,  # the max ngram length to index one
        i_min_matches=100,  # minimum similar strings to compare, no matter how disimilar they are
        i_max_matches=100,  # if this many matches at the longest ngram length, don't decrement length and add more
        
        b_already_clean=False,  # passing in an already cleaned list of texts (unicode, punctuation, etc)
        b_phonetic_index=False,  # count character ngrams after converting string to phonetic. Numbers must change if this True
        b_ngrams_cross_boundaries=False,  # also use ngrams that cross word boundaries like "nd ano"
                                          # only applies if not using phonetics
        
        i_max_start_len=10,  # maximum length to look at for considering whether the starts match. -1 is whole length of shorter text
        i_max_end_len=10,  # maximum length to look at for considering whether the ends match. -1 is whole length of shorter text
        
        d_length_weight = 0.25,  # the proportion of the score based on having the same length
        d_edit_weight = 1.05,  # the proportion of the score based on number of required edits
        d_ngram_weight = 1.25,  # the proportion of the score based on having the same character ngrams
        d_phonetic_weight = 1.0,  # the proportion of the score based on sounding the same
        d_start_weight = 0.50,  # the proportion of the score based on starting with the same text
        d_end_weight = 0.20,  # the proportion of the score based on ending with the same text
        d_number_weight = 1.15,  # the proportion of the score from the numbers being the same
        d_contains_search_weight = 0.75,  # the proportion of the score from the words in the search all being in the potential match
        d_contains_match_weight = 0.75,  # the proportion of the score from the words of the potential match all being in the search
        b_cache_searches = False,  # store searches for faster lookup the next time at the cost of ram usage
        a_unimportant_words = [], # words that don't matter at all for considering overlap between search and match
        a_less_important_words = [], # words that only matter a little for considering overlap between search and match
        d_important_word_factor = 0.75, # what percentage of the contains overlap comes from just the important words in common
    ):
        '''
            Arguments:
                texts,  # list of texts to compare to
                i_min_ngram_chars: {int}, default 5, minimum character length before comparing
                i_max_ngram_chars: {int}, default 25, the max ngram length to index one
                i_min_matches: {int}, default 100, minimum similar strings to compare, no matter how disimilar they are
                i_max_matches: {int}, default 1000, if this many matches at the longest ngram length, don't decrement length and add more
                
                b_already_clean: {bool}, default False, if passing in an already cleaned list of texts (unicode, punctuation, etc)
                b_phonetic_index: {bool}, default False, whether to count character ngrams after converting string to phonetic.
                                  Numbers must change if this True
                b_ngrams_cross_boundaries: {bool}, default False also use ngrams that cross word boundaries like "nd ano"
                                           only applies if not using phonetics
                
                i_max_start_len: {int}  # maximum length to look at for considering whether the starts match. -1 is whole length of shorter text
                i_max_end_len {int},  # maximum length to look at for considering whether the ends match. -1 is whole length of shorter text
                
                d_length_weight: {float}, default 0.25, the proportion of the score based on having the same length
                d_edit_weight: {float}, default 1.05, the proportion of the score based on number of required edits
                d_ngram_weight: {float}, default 1.25, the proportion of the score based on having the same character ngrams
                d_phonetic_weight: {float}, default 1.0, the proportion of the score based on sounding the same
                d_start_weight: {float}, default 0.50, the proportion of the score based on starting with the same text
                d_end_weight: {float}, default 0.20, the proportion of the score based on ending with the same text
                d_number_weight: {float}, default 1.15, the proportion of the score from the numbers being the same
                d_contains_search_weight: {float}, default 0.75, the proportion of the score from the words in the search all being in the potential match
                d_contains_match_weight: {float}, default 0.75, the proportion of the score from the words of the potential match all being in the search
                
                b_cache_searches: {bool} store searches for faster lookup the next time at the cost of ram usage
                a_unimportant_words: {list} or {set} of words that don't matter at all for considering overlap between search and match
                a_less_important_words: {list} or {set} of words that only matter a little for considering overlap between search and match
                d_important_word_factor: {float} the percentage of the contains overlap comes from just the important words in common
        '''
        self.texts = []  # list of original texts
        self.bare = []  # list of cleaned version (minus punctuation, accents)
        self.phon = []  # phonetic versions
        self.ngrams = defaultdict(defaultdict)  # length: ngram: [score, uniqueness, char_value, length, {member_indices}]
        # {
        #    2: {
        #        'ab': {idx1, idx3, idx6...},
        #        'bc': {idx3, idx6...},
        #        ...
        #    },
        #    3: {
        #        'abc': {idx3, idx27},
        #        'abd': {idx6, idx14...},
        #        ...
        #    },
        #    ...
        #}
        
        self._min_ngram_chars, self._max_ngram_chars = i_min_ngram_chars, i_max_ngram_chars
        self._min_matches, self._max_matches = i_min_matches, i_max_matches
        
        self._already_clean = b_already_clean
        self._phonetic_index = b_phonetic_index
        self._ngrams_cross_boundaries = b_ngrams_cross_boundaries
        self._max_start_len, self._max_end_len = i_max_start_len, i_max_end_len
        
        self._length_weight = d_length_weight
        self._edit_weight = d_edit_weight
        self._ngram_weight = d_ngram_weight
        self._phonetic_weight = d_phonetic_weight
        self._start_weight = d_start_weight
        self._end_weight = d_end_weight
        self._number_weight = d_number_weight
        self._contains_search_weight = d_contains_search_weight
        self._contains_match_weight = d_contains_match_weight
        
        d_contains_search = 0.75,  # the proportion of the score from the words in the search all being in the potential match
        d_contains_match = 0.75,  # the proportion of the score from the words of the potential match all being in the search
        #print('self._max_ngram_chars', self._max_ngram_chars)
        self._ngram_len_scores = {i: math.log10(i) * 0.5 for i in range (1, self._max_ngram_chars + 1)}
        
        self.add_texts(texts)
        
        self.cache = {}
        self._cache_searches = b_cache_searches
        
        self._unimportant_words = set([str(word).lower() for word in a_unimportant_words])
        self._less_important_words  = set([str(word).lower() for word in a_less_important_words])
        self._important_word_factor = float(d_important_word_factor)
    
    def _unload(self):
        self.texts = []  # list of original texts
        self.bare = []  # list of cleaned version (minus punctuation, accents)
        self.phon = []  # phonetic versions
        self.ngrams = defaultdict(defaultdict)  # length: ngram: [score, uniqueness, char_value, length, {member_indices}]
    
    def _bare_clean(self, text):
        return remove_all_punctuation(
            clean_text(
                re.sub("-", " ", text),
                b_accents=True,
                b_lowercase=True,
                b_punctuation=True,
                b_trademarks=True,
                b_html_characters=True,
                b_ampersands=True,
                b_periods=True,
            ),
            b_keepspaces=True,
            b_dehyphenate_phrases=False,)
    
    def _get_ngrams(self, s_text):
        if self._ngrams_cross_boundaries or self._phonetic_index:
            return character_ngrams(list(s_text), self._max_ngram_chars, self._min_ngram_chars)
        else:
            a_ngrams = []
            for s_word in s_text.split():
                a_ngrams.extend(character_ngrams(list(s_word), self._max_ngram_chars, self._min_ngram_chars))
            return a_ngrams
    
    def _get_ngram_score(self, s_ngram, a_ngram_data):
        ''' the score is based on the length of the ngram,
            its character values, and its uniqueness
            Arguments:
                s_ngram: {str} of ngram that is in the indexed ngrams
                a_ngram_date: {list} of [score, uniqueness, char_value, length, {member_indices}]
        '''
        cdef float d_char_score, d_uniqueness, d_members, d_coverage, d_len_score, d_score
        cdef int i_members
        
        d_char_score = a_ngram_data[2] = get_char_values(s_ngram) / (i_MAX_CHAR_VAL * self._max_ngram_chars)
        
        i_members = len(a_ngram_data[4])
        d_uniqueness = 1 - min(1.0, (i_members - 1 / 25)) if i_members > 1 else 1.0
        
        d_members = min(1.0, (i_members / (len(self.texts) / 10.0)))
        d_coverage = a_ngram_data[1] = (1 - d_members) ** 4

        d_len_score = self._ngram_len_scores[min(self._max_ngram_chars, a_ngram_data[3])]
        d_score = a_ngram_data[0] = sum([
            d_uniqueness * 2,
            d_coverage * 2,
            d_char_score * 2,
            d_len_score,
        ]) / 7
        return d_score
    
    def _get_non_indexed_ngram_score(self, s_ngram):
        ''' the score is based on the length of the ngram,  its character values
            Arguments:
                s_ngram: {str} of ngram that isn't in the indexed ngrams
        '''
        cdef d_char_score, d_len_score, d_score
        d_char_score = get_char_values(s_ngram) / (i_MAX_CHAR_VAL * self._max_ngram_chars)
        d_len_score = self._ngram_len_scores[min(self._max_ngram_chars, len(s_ngram))]
        d_score = sum([
            d_char_score * 2,
            d_len_score,
        ]) / 3
        return d_score
    
    def add_texts(self, a_texts, b_clear_old=False, b_already_clean=False):
        ''' add list of new texts to internal lists and indexes. Texts should already be unique.
            Arguments:
                a_texts: {list} of string text values
                b_clear_old: {bool} clear out existing lists
                b_already_clean: {bool} the strings are already cleaned of punctuation/accents
        '''
        cdef str s_text, s_ngram
        cdef int i_len
        #cdef dict e_ngrams
        cdef list a_ngram_data
        
        if b_clear_old: self._unload()
        if not a_texts:
            print("No texts passed in to add.")
            return self
        
        for s_text in a_texts:
            self.add_text(s_text, b_already_clean, b_update_score=False)
        
        for i_len, e_ngrams in self.ngrams.items():
            for s_ngram, a_ngram_data in e_ngrams.items():
                self._get_ngram_score(s_ngram, a_ngram_data)
        
        return self
    
    def add_text(self, s_text, b_already_clean=False, b_update_score=True):
        ''' add new text to internal lists and indexes. Text should already be unique.
            Arguments:
                s_text: {str} of individual text
                b_already_clean: {bool} the string is are already cleaned of punctuation/accents
        '''
        cdef int idx, i_len
        cdef str s_bare, s_phon, s_ngram
        cdef set c_ngrams
        
        idx = len(self.texts)
        self.texts.append(s_text)
        
        s_bare = s_text if b_already_clean else self._bare_clean(s_text)
        self.bare.append(s_bare)
        
        try: s_phon = o_METAPHONE.phonetics(s_bare)
        except: s_phon = s_bare
        self.phon.append(s_phon)
        
        if self._phonetic_index:
            c_ngrams = set(self._get_ngrams(s_phon))
        else:
            c_ngrams = set(self._get_ngrams(s_bare))
        
        for s_ngram in c_ngrams:
            i_len = len(s_ngram)
            if s_ngram not in self.ngrams[i_len]:
                self.ngrams[i_len][s_ngram] = [-1.0, -1.0, get_char_values(s_ngram), i_len, set()]
            
            self.ngrams[i_len][s_ngram][4].add(idx)
            
            if b_update_score:
                self._get_ngram_score(s_ngram, self.ngrams[i_len][s_ngram])
    
    def find_ngrams(
        self, s_text, s_bare='', s_phon='', b_already_clean=False,
        i_min_ngrams=0, i_max_ngrams=0,
        d_min_ngram_score=0.2,
        i_min_candidates=2, i_max_candidates=1000
    ):
        cdef set c_ngrams, c_matches, c_non_indexed_ngrams
        cdef list a_found_ngrams, a_ngram_value, a_ngrams
        cdef int i_ngrams, i_min_ngs, i_max_ngs, i_min, i_max
        cdef float d_max_score, d_minimums, d_min_ng_score
        cdef str s_ngram
        cdef bint b_minimums
        cdef tuple t_match
        
        i_min_ngs, i_max_ngs = i_min_ngrams, i_max_ngrams
        i_min, i_max = i_min_candidates, i_max_candidates
        d_min_ng_score = d_min_ngram_score
        
        if not s_bare:
            s_bare = s_text if b_already_clean else self._bare_clean(s_text)
        if not s_phon:
            try: s_phon = o_METAPHONE.phonetics(s_bare)
            except: s_phon = s_bare

        if self._phonetic_index:
            c_ngrams = set(self._get_ngrams(s_phon))
        else:
            c_ngrams = set(self._get_ngrams(s_bare))

        a_found_ngrams, c_non_indexed_ngrams = [], set()
        i_ngrams, c_matches, d_max_score, b_minimums = 0, set(), 0.0, False

        for s_ngram in sorted(c_ngrams, key=lambda a_ngrams: len(a_ngrams), reverse=True):
            a_ngram_value = self.ngrams[len(s_ngram)].get(s_ngram)
            if not a_ngram_value:
                c_non_indexed_ngrams.add(s_ngram)
                continue  # non-indexed ngram

            #print(s_ngram, f"i_ngrams={i_ngrams}; c_matches={len(c_matches)}; ; d_max_score={d_max_score}")
            if b_minimums or (i_ngrams > i_min_ngs and len(c_matches) > i_min_candidates and d_max_score > d_min_ng_score):
                if not b_minimums:
                    #print('minimums met')
                    b_minimums = True
                if (
                    (i_ngrams >= i_max_ngs and i_max_ngs > 0) or
                    (i_max > 0 and len(c_matches) >= i_max)):
                    #print('maximums met')
                    break  # if minimums are all met, check if any maximums are met
                if a_ngram_value[0] < d_min_ngram_score:
                    continue  # if minimums are already met, skip really low score ngrams
            d_max_score = max(d_max_score, a_ngram_value[0])

            i_ngrams += 1
            a_found_ngrams.append((s_ngram, a_ngram_value))
            c_matches = c_matches.union(a_ngram_value[-1])

        a_found_ngrams.sort(key=lambda t_match: t_match[1][0], reverse=True)
        if i_max_ngs > 1:
            a_found_ngrams = a_found_ngrams[:i_max_ngs]

        return c_ngrams, a_found_ngrams, c_non_indexed_ngrams, c_matches
    
    def sort_candidates(self, a_found_ngrams):
        ''' get the best candidates out of the those that have ngram overlap '''
        cdef str s_ngram
        cdef list a_ngram_data
        cdef float d_ngram_score
        cdef set c_ngram_members
        cdef int i_candidate
        
        e_candidates = defaultdict(dict)
        for s_ngram, a_ngram_data in a_found_ngrams:
            d_ngram_score, c_ngram_members = a_ngram_data[0], a_ngram_data[-1]
            for i_candidate in c_ngram_members:
                if i_candidate not in e_candidates:
                    e_candidates[i_candidate] = {
                        'ngram_overlap': 0,
                        'matched_indexed_score': 0.0,
                        'ngrams': {},
                    }

                e_candidates[i_candidate]['ngram_overlap'] += 1
                e_candidates[i_candidate]['matched_indexed_score'] += d_ngram_score
                e_candidates[i_candidate]['ngrams'][s_ngram] = a_ngram_data[:-1]

        a_best_candidates = sorted(e_candidates.items(), key=lambda t_cand: t_cand[1]['matched_indexed_score'], reverse=True)
        return a_best_candidates
    
    def score_candidates(self, c_search_ngrams, a_candidates, a_found_ngrams, c_non_indexed_ngrams):
        cdef float d_non_indexed, d_indexed_ngrams, d_total_search_ngrams, d_ngram
        cdef float d_matched_score, d_unmatched_score, d_jaccard
        cdef list a_scored_candidates
        cdef int idx, i_len, i_matched_ngrams, i_just_search_ngrams, i_just_candidate_ngrams
        cdef str s_candidate, s_ngram
        cdef dict e_candidate, e_ngrams
        cdef set c_candidate_ngrams
        
        # the scores of the search ngrams that don't exist at all in the indexes
        d_non_indexed = sum([
            self._get_non_indexed_ngram_score(s_ngram)
            for s_ngram in c_non_indexed_ngrams])

        # the scores of all the search ngrams that matched anything
        d_indexed_ngrams = sum([
            a_ngram_data[0]
            for s_ngram, a_ngram_data in a_found_ngrams
            if s_ngram not in c_non_indexed_ngrams])

        d_total_search_ngrams = d_non_indexed + d_indexed_ngrams

        a_scored_candidates = []
        for idx, e_candidate in a_candidates:
            s_candidate = self.texts[idx]
            if self._phonetic_index:
                c_candidate_ngrams = set(self._get_ngrams(self.phon[idx]))
            else:
                c_candidate_ngrams = set(self._get_ngrams(self.bare[idx]))

            e_ngrams = {}
            e_scores = Counter()
            i_matched_ngrams, i_just_search_ngrams, i_just_candidate_ngrams = 0, 0, 0
            for s_ngram in c_search_ngrams:
                i_len = len(s_ngram)
                if s_ngram in c_candidate_ngrams:
                    i_matched_ngrams += 1
                    if s_ngram in self.ngrams[i_len]:
                        d_ngram = self.ngrams[i_len][s_ngram][0]
                        e_ngrams[s_ngram] = ('matched', 'indexed', d_ngram)
                        e_scores[('matched', 'indexed')] += d_ngram
                    else:
                        d_ngram = self._get_non_indexed_ngram_score(s_ngram)
                        e_ngrams[s_ngram] = ('matched', 'non_indexed', d_ngram)
                        e_scores[('matched', 'non_indexed')] += d_ngram
                else:
                    i_just_search_ngrams += 1
                    if s_ngram in self.ngrams[i_len]:
                        d_ngram = self.ngrams[i_len][s_ngram][0]
                        e_ngrams[s_ngram] = ('unmatched_search', 'indexed', d_ngram)
                        e_scores[('unmatched_search', 'indexed')] += d_ngram
                    else:
                        d_ngram = self._get_non_indexed_ngram_score(s_ngram)
                        e_ngrams[s_ngram] = ('unmatched_search', 'non_indexed', d_ngram)
                        e_scores[('unmatched_search', 'non_indexed')] += d_ngram

            for s_ngram in c_candidate_ngrams:
                i_len = len(s_ngram)
                if s_ngram not in c_search_ngrams:
                    i_just_candidate_ngrams += 1
                    if s_ngram in self.ngrams[i_len]:
                        d_ngram = self.ngrams[i_len][s_ngram][0]
                        e_ngrams[s_ngram] = ('unmatched_candidate', 'indexed', d_ngram)
                        e_scores[('unmatched_candidate', 'indexed')] += d_ngram
                    else:
                        d_ngram = self._get_non_indexed_ngram_score(s_ngram)
                        e_ngrams[s_ngram] = ('unmatched_candidate', 'non_indexed', d_ngram)
                        e_scores[('unmatched_candidate', 'non_indexed')] += d_ngram


            d_matched_score = sum([
                e_scores.get(('matched', 'indexed'), 0.0),
                e_scores.get(('matched', 'non_indexed'), 0.0),
            ]) / 2

            d_unmatched_score = sum([
                e_scores.get(('unmatched_search', 'indexed'), 0.0),
                e_scores.get(('unmatched_search', 'non_indexed'), 0.0),
                e_scores.get(('unmatched_candidate', 'indexed'), 0.0),
                e_scores.get(('unmatched_candidate', 'non_indexed'), 0.0),
            ]) / 4
            d_jaccard = d_matched_score / (d_matched_score + d_unmatched_score)

            a_scored_candidates.append({
                'idx': idx,
                'text': self.texts[idx],
                'total_ngrams': i_matched_ngrams + i_just_search_ngrams + i_just_candidate_ngrams,
                'matched_ngrams': i_matched_ngrams,
                'just_search_ngrams': i_just_search_ngrams,
                'just_candidate_ngrams': i_just_candidate_ngrams,

                'matched_score': d_matched_score,
                'unmatched_score': d_unmatched_score,
                'ngram_score': d_jaccard,
                'score': d_jaccard,
            })


        a_scored_candidates.sort(key=lambda x: x['score'], reverse=True)
        return a_scored_candidates
    
    def score_matches(self, text, a_matches, int i_max_matches=-1, float d_percent_advanced=0.75):
        cdef dict e_match
        cdef float d_existing_score
        
        for e_match in a_matches:
            d_existing_score = e_match['score']
            e_match['score'] = sum([
                d_existing_score * (1 - d_percent_advanced),
                self._compare(text, e_match['text']) * d_percent_advanced
            ])
        a_matches.sort(key=lambda e: e['score'], reverse=True)
        if i_max_matches > 0:
            return a_matches[:i_max_matches]
        else:
            return a_matches
    
    def search(
        self, text, b_already_clean=False,
        i_min_ngrams=0, i_max_ngrams=25, d_min_ngram_score=0.1,
        i_min_candidates=2, i_max_candidates=250, i_max_matches=50,
        d_percent_advanced=0.75):
        cdef set c_ngrams, c_non_indexed_ngrams, c_potential_matches 
        cdef list a_found_ngrams, a_best_candidates, a_matches
        
        if self._cache_searches and text in self.cache:
            return self.cache[text]
        
        c_ngrams, a_found_ngrams, c_non_indexed_ngrams, c_potential_matches = self.find_ngrams(
            text, b_already_clean=b_already_clean,
            i_min_ngrams=i_min_ngrams, i_max_ngrams=i_max_ngrams,
            d_min_ngram_score=d_min_ngram_score,
            i_min_candidates=i_min_candidates, i_max_candidates=i_max_candidates)

        a_best_candidates = self.sort_candidates(a_found_ngrams)
        a_matches = self.score_candidates(c_ngrams, a_best_candidates, a_found_ngrams, c_non_indexed_ngrams)
        if d_percent_advanced > 0:
            a_matches = self.score_matches(text, a_matches, i_max_matches=i_max_matches, d_percent_advanced=d_percent_advanced)
            
        
        if self._cache_searches:
            self.cache[text] = a_matches
        
        return a_matches
    
    def find(
        self, text, b_already_clean=False,
        i_min_ngrams=0, i_max_ngrams=25, d_min_ngram_score=0.1, i_min_matches=2, 
        i_min_candidates=2, i_max_candidates=250, i_max_matches=50, d_percent_advanced=0.75):
        cdef list a_matches
        a_matches = self.search(
            text, b_already_clean=b_already_clean,
            i_min_ngrams=i_min_ngrams, i_max_ngrams=i_max_ngrams,
            i_min_candidates=i_min_candidates, i_max_candidates=i_max_candidates,
            i_max_matches=i_max_matches, d_percent_advanced=d_percent_advanced)
        
        if a_matches:
            return (a_matches[0]["text"], a_matches[0]["score"])
        else:
            return None
    
    def compare(self, s_text1, s_text2):
        s_text1 = str(s_text1)
        s_text2 = str(s_text2)
        return self._compare(s_text1, s_text2)
    
    cpdef float _compare(self, str s_text1, str s_text2, str s_phon1='', str s_phon2=''):
        cdef int i_len, i_len1, i_len2, i_edits, i_phonetic, i_start_len, i_end_len, i_start, i_end
        cdef str s_lower1, s_lower2, s_punc1, s_punc2
        cdef float d_similarity, d_len, d_lev, d_phonetic, d_jaccard, d_start, d_end
        cdef float d_sum1, d_sum2, d_numbers_jaccard, d_numbers_jaccard1, d_numbers_value, d_number
        cdef list a_numbers1, a_numbers2, as_numbers1, as_numbers2, a_similarities, a_words1, a_words2
        cdef set c_text1, c_text2, c_common
        cdef tuple t_sim, t_wts, t_scores
        
        if not s_text1 or not s_text2: return 0.0
        a_similarities = []
        
        s_lower1, s_lower2 = s_text1.lower(), s_text2.lower()
        i_len1, i_len2 = len(s_text1), len(s_text2)
        
        if self._length_weight > 0:
            d_len = min(i_len1, i_len2) / float(max(i_len1, i_len2))
            a_similarities.append((d_len, self._length_weight, d_len * self._length_weight))
        
        
        # levenshtein-damerau similarity
        if self._edit_weight > 0:
            tn_edits = edits(s_text1, s_text2, ignore_punctuation=True)
            i_edits = sum([len(x) for x in tn_edits])
            d_lev = 1 - (i_edits / max(i_len1, i_len2))
            a_similarities.append((d_lev, self._edit_weight, d_lev * self._edit_weight))
        
        # phonetic similarity
        if not s_phon1:
            try: s_phon1 = o_METAPHONE.phonetics(self._bare_clean(s_text1))
            except: s_phon1 = ''
        if not s_phon2:
            try: s_phon2 = o_METAPHONE.phonetics(self._bare_clean(s_text2))
            except: s_phon2 = ''
        
        if self._phonetic_weight > 0:
            if s_phon1 and s_phon2:
                i_phonetic = distance(s_phon1, s_phon2)
                d_phonetic = 1 - (i_phonetic / float(max(len(s_phon1), len(s_phon2))))
            else:
                d_phonetic = 0.0
            a_similarities.append((d_phonetic, self._phonetic_weight, d_phonetic * self._phonetic_weight))
        
        s_punc1 = re_PUNCT.sub("", s_lower1)
        s_punc2 = re_PUNCT.sub("", s_lower2)
        # character ngram similarity
        if self._ngram_weight > 0:
            d_jaccard = jaccard(
               character_ngrams(list(s_punc1), i_max_size=4, i_min_size=1),
               character_ngrams(list(s_punc2), i_max_size=4, i_min_size=1))
            a_similarities.append((d_jaccard, self._ngram_weight, d_jaccard * self._ngram_weight))
        
        i_len = min(i_len1, i_len2)
        i_punct_len = min(len(s_punc1), len(s_punc2))
        # start similarity
        d_start = 0.0
        if self._start_weight > 0:
            i_start_len = min(self._max_start_len, i_punct_len) if self._max_start_len > 0 else i_punct_len
            
            for i_start in reversed(range(1, i_start_len + 1)):
                #print(f'"{s_text1[:i_start]}", "{s_text2[:i_start]}"')
        
                if s_punc1[:i_start] == s_punc2[:i_start]:
                    d_start = i_start / float(i_start_len)
                    break
            a_similarities.append((d_start, self._start_weight, d_start * self._start_weight))
            
        # end similarity
        d_end = 0.0
        if self._end_weight > 0:
            i_end_len = min(self._max_end_len, i_punct_len) if self._max_end_len > 0 else i_punct_len
            
            for i_end in reversed(range(1, i_end_len + 1)):
                #print(f'"{s_text1[:i_end]}", "{s_text2[:i_end]}"')
                if s_punc1[-i_end:] == s_punc2[-i_end:]:
                    d_end = i_end / float(i_end_len)
                    break
            a_similarities.append((d_end, self._end_weight, d_end * self._end_weight))
            
        
        # search in match and match in search
        if self._contains_search_weight > 0 or self._contains_match_weight > 0:
            a_words1, a_words2 = s_lower1.split(), s_lower2.split()
        
            if self._unimportant_words:
                a_words1 = [word for word in a_words1 if word not in self._unimportant_words]
                a_words2 = [word for word in a_words2 if word not in self._unimportant_words]
        
            c_text1, c_text2 = set(a_words1), set(a_words2)
            
            c_common = c_text1.intersection(c_text2)
            d_match_contains_search = len(c_common) / len(c_text1) if c_text1 else 0.0
            d_search_contains_match = len(c_common) / len(c_text2) if c_text2 else 0.0
            
            if self._less_important_words:
                c_text1 = set([word for word in a_words1 if word not in self._less_important_words])
                c_text2 = set([word for word in a_words2 if word not in self._less_important_words])
                c_common = c_text1.intersection(c_text2)
                if c_text1:
                    d_match_contains_search = (
                        ((len(c_common) / len(c_text1)) * self._important_word_factor) +
                        (d_match_contains_search * (1 - self._important_word_factor))
                    )
                else:
                    d_match_contains_search = (
                        0.0 +
                        (d_match_contains_search * (1 - self._important_word_factor))
                    )
                if c_text2:
                    d_search_contains_match = (
                        ((len(c_common) / len(c_text2)) * self._important_word_factor) +
                        (d_search_contains_match * (1 - self._important_word_factor))
                    )
                else:
                    d_search_contains_match = (
                        0.0 +
                        (d_search_contains_match * (1 - self._important_word_factor))
                    )
        
            a_similarities.append((d_match_contains_search, self._contains_search_weight, d_match_contains_search * self._contains_search_weight))
            a_similarities.append((d_search_contains_match, self._contains_match_weight, d_search_contains_match * self._contains_match_weight))
        
        # matching numbers
        if self._number_weight > 0:
            as_numbers1 = get_numbers(s_text1)
            as_numbers2 = get_numbers(s_text2)
            a_numbers1 = [to_float(x) for x in as_numbers1]
            a_numbers2 = [to_float(x) for x in as_numbers2]
            d_numbers_jaccard = jaccard(as_numbers1, as_numbers2)
            d_numbers_jaccard1 = jaccard(a_numbers1, a_numbers2)
            d_sum1, d_sum2 = sum(a_numbers1), sum(a_numbers2)
            d_numbers_value = (min(d_sum1, d_sum2) / max(d_sum1, d_sum2)) ** 2 if d_sum1 + d_sum2 > 0 else 1.0
            d_number = (d_numbers_jaccard + d_numbers_jaccard1 + d_numbers_value) / 3
            #print('d_number', d_number, s_text1, s_text2)
            a_similarities.append((d_number, self._number_weight, d_number * self._number_weight))
        
        a_similarities.sort(reverse=True, key=itemgetter(0))
        t_sim, t_wts, t_scores = zip(*a_similarities)
        d_similarity = sum(t_scores[:-1]) / sum(t_wts[:-1])

        return d_similarity
    
    cpdef dict _similarities(self, str s_text1, str s_text2, str s_phon1='', str s_phon2=''):
        cdef int i_len1, i_len2, i_edits, i_phonetic
        cdef str s_lower1, s_lower2, s_punc1, s_punc2
        cdef float d_similarity, d_len, d_lev, d_phonetic, d_jaccard, d_start, d_end
        cdef float d_sum1, d_sum2, d_numbers_jaccard, d_numbers_jaccard1, d_numbers_value, d_number
        cdef list a_numbers1, a_numbers2, as_numbers1, as_numbers2, a_similarities, a_words1, a_words2
        cdef dict e_similarities
        cdef tuple t_sim, t_wts, t_scores
        
        if not s_text1 or not s_text2: return 0.0
        i_len1, i_len2 = len(s_text1), len(s_text2)
        d_len = min(i_len1, i_len2) / float(max(i_len1, i_len2))
        s_lower1, s_lower2 = s_text1.lower(), s_text2.lower()
        
        # levenshtein-damerau similarity
        tn_edits = edits(s_text1, s_text2, ignore_punctuation=True)
        i_edits = sum([len(x) for x in tn_edits])
        d_lev = 1 - (i_edits / max(i_len1, i_len2))
        
        # phonetic similarity
        if not s_phon1:
            try: s_phon1 = o_METAPHONE.phonetics(self._bare_clean(s_text1))
            except: s_phon1 = ''
        if not s_phon2:
            try: s_phon2 = o_METAPHONE.phonetics(self._bare_clean(s_text2))
            except: s_phon2 = ''
        
        if s_phon1 and s_phon2:
            i_phonetic = distance(s_phon1, s_phon2)
            d_phonetic = 1 - (i_phonetic / float(max(len(s_phon1), len(s_phon2))))
        else:
            d_phonetic = 0.0
        
        s_punc1 = re_PUNCT.sub("", s_lower1)
        s_punc2 = re_PUNCT.sub("", s_lower2)
        
        # character ngram similarity
        d_jaccard = jaccard(
           character_ngrams(list(s_punc1), i_max_size=4, i_min_size=1),
           character_ngrams(list(s_punc2), i_max_size=4, i_min_size=1))
        
        # start similarity
        i_start_len = min(self._max_start_len, i_len1, i_len2) if self._max_start_len > 0 else min(i_len1, i_len2)
        if s_text1[:i_start_len] == s_text2[:i_start_len] or s_lower1[:i_start_len] == s_lower2[:i_start_len]:
            d_start = 1.0
        else:
            d_start = 0.0
            
        # end similarity
        i_end_len = min(self._max_end_len, i_len1, i_len2) if self._max_end_len > 0 else min(i_len1, i_len2)
        if s_text1[-i_end_len:] == s_text2[-i_end_len:] or s_lower1[-i_end_len:] == s_lower2[-i_end_len:]:
            d_end = 1.0
        else:
            d_end = 0.0
        
        as_numbers1 = get_numbers(s_text1)
        as_numbers2 = get_numbers(s_text2)
        a_numbers1 = [to_float(x) for x in as_numbers1]
        a_numbers2 = [to_float(x) for x in as_numbers2]
        d_numbers_jaccard = jaccard(as_numbers1, as_numbers2)
        d_numbers_jaccard1 = jaccard(a_numbers1, a_numbers2)
        d_sum1, d_sum2 = sum(a_numbers1), sum(a_numbers2)
        d_numbers_value = (min(d_sum1, d_sum2) / max(d_sum1, d_sum2)) ** 2 if d_sum1 + d_sum2 > 0 else 1.0
        d_number = (d_numbers_jaccard + d_numbers_jaccard1 + d_numbers_value) / 3
        #print('d_number', d_number, s_text1, s_text2)
        
        e_similarities = {
            "length": (d_len, self._length_weight, d_len * self._length_weight),
            "levenshtein": (d_lev, self._edit_weight, d_lev * self._edit_weight),
            "jaccard": (d_jaccard, self._ngram_weight, d_jaccard * self._ngram_weight),
            "phonetic": (d_phonetic, self._phonetic_weight, d_phonetic * self._phonetic_weight),
            "starts with": (d_start, self._start_weight, d_start * self._start_weight),
            "ends with": (d_end, self._end_weight, d_end * self._end_weight),
            "number": (d_number, self._number_weight, d_number * self._number_weight),
        }
        a_similarities = list(e_similarities.values())
        a_similarities.sort(reverse=True, key=itemgetter(0))
        t_sim, t_wts, t_scores = zip(*a_similarities)
        e_similarities["similarity"] = sum(t_scores[:-1]) / sum(t_wts[:-1])
        
        return e_similarities
