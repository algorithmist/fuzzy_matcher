import numpy as np
import pyximport; pyximport.install(
    setup_args={'include_dirs': np.get_include()},
    reload_support=True,
    language_level=2)
#import pyximport; pyximport.install()

try:
    from . import cleaning
    from . import utils
except ImportError:
    import cleaning
    import utils

try:
    from fuzzy_texts import *
except ImportError:
    from .fuzzy_texts import *


__version__ = "0.2.2"

