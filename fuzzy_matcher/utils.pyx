﻿#from cpython cimport array
#import array
cimport numpy as np
import numpy as np

import re, math
from collections import defaultdict, Counter
from operator import itemgetter
from string import punctuation
from time import time

try:
    from .cleaning import strip_unicode_accents
except:
    from cleaning import strip_unicode_accents


cdef dict e_CHAR_IMPORTANCE = {
    'e': <int>1, 'E': <int>11,
    'a': <int>1, 'A': <int>11,
    'i': <int>2, 'I': <int>12,
    'o': <int>3, 'O': <int>13,
    'u': <int>3, 'U': <int>13,
    'y': <int>4, 'Y': <int>14,
    'r': <int>25, 'R': <int>35,
    'l': <int>26, 'L': <int>36,
    's': <int>27, 'S': <int>37,
    'z': <int>27, 'Z': <int>37,
    'c': <int>28, 'C': <int>38,
    'k': <int>28, 'K': <int>38,
    'q': <int>29, 'Q': <int>39,
    'd': <int>31, 'D': <int>41,
    'b': <int>31, 'B': <int>41,
    'p': <int>31, 'P': <int>41,
    't': <int>31, 'T': <int>41,
    'm': <int>32, 'M': <int>42,
    'n': <int>32, 'N': <int>42,
    'h': <int>32, 'H': <int>42,
    'g': <int>33, 'G': <int>43,
    'j': <int>33, 'J': <int>43,
    'f': <int>34, 'F': <int>44,
    'v': <int>35, 'V': <int>45,
    'w': <int>35, 'W': <int>45,
    'x': <int>36, 'X': <int>46,
    '0': <int>50,
    '1': <int>51,
    '2': <int>52,
    '3': <int>53,
    '4': <int>54,
    '5': <int>55,
    '6': <int>56,
    '7': <int>57,
    '8': <int>58,
    '9': <int>59,
}

e_CHAR_IMPORTANCE.update({s_p: 0 for s_p in punctuation})
cdef int i_MAX_CHAR_VAL = max(e_CHAR_IMPORTANCE.values())
CHAR_IMPORTANCE = dict(e_CHAR_IMPORTANCE)

cdef set c_IMPORTANT = set('0123456789Xx')
cdef set c_IMPORTANT_CHANGE = set('0123456789Xx')
cdef set c_OTHER = set('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrtuvwxyz.')
cdef set c_UNIMPORTANT = set('s _-,()\'"')

re_PUNCT = re.compile(r"[^A-Za-z0-9]")

re_FRACTION = re.compile(r'(?P<sign>[+-]?)((?P<whole>\d+)([ +-]| [+-] ))?(?P<num>\d+)/(?P<denom>[1-9]\d*)')
re_DECIMAL = re.compile(r'(?P<sign>[+-]?)(?P<thousands>(\d{1,6},)+)?(?P<whole>(?<!\.)\d+)?(?P<dec>\.\d+)?(?P<percent> ?%)?')


cpdef double to_float(v_fraction):
    ''' Take a fraction and convert it into a float.
        Can accept formats like "-1/2", "11-1/2", "-11-1/2", "11 1/2", "11 1/2"
    '''
    cdef dict e_fract
    cdef double d_whole, d_num, d_denom, d_dec, d_value
    cdef int i_sign
    
    if isinstance(v_fraction, str):
        o_match = re_FRACTION.search(v_fraction.strip())
        if o_match:
            e_fract = o_match.groupdict()

            if 'num' in e_fract and 'denom' in e_fract:
                if e_fract.get('whole', 0.0):
                    d_value = float(e_fract['whole'])
                else:
                    d_value = 0.0

                d_num = float(e_fract.get('num', 0.0))
                d_denom = float(e_fract.get('denom', 0.0))
                if d_denom > 0:
                    d_value += (d_num / d_denom)

                if d_value > 0:
                    if e_fract.get('sign') == '-':
                        return -d_value
                    else:
                        return d_value
                else:
                    return np.nan
            else:
                return np.nan
            
        else:
            o_match = re_DECIMAL.search(v_fraction.strip())
            if o_match:
                e_fract = o_match.groupdict()
                if 'whole' not in e_fract and 'dec' not in e_fract:
                    return np.nan
                d_value = 0
                
                i_digits = 0
                if e_fract.get('whole', 0.0):
                    d_value += float(e_fract['whole'])
                    i_digits = len(e_fract['whole'])
                    
                if e_fract.get('thousands', 0.0):
                    d_value += float(e_fract['thousands'].replace(",", "")) * (10**i_digits)
                    
                if e_fract.get('dec', 0.0):
                    d_value += float(e_fract['dec'])
                
                if e_fract.get('percent'):
                    d_value /= 100
                
                if e_fract.get('sign') == '-':
                    return -d_value
                else:
                    return d_value
    
    elif isinstance(v_fraction, (float, np.float32)):
        return v_fraction
    elif isinstance(v_fraction, int):
        return float(v_fraction)
    else:
        return np.nan


cpdef list get_numbers(str s_text):
    ''' get a list of the numbers that appear in a string as whole, fractions or decimals '''
    cdef tuple t_span
    cdef list a_numbers = []
    if not s_text: return a_numbers
    for re_num in re_FRACTION.finditer(s_text):
        a_numbers.append(re_num.span())
    for re_num in re_DECIMAL.finditer(s_text):
        a_numbers.append(re_num.span())
    a_numbers = [
        s_text[t_span[0]: t_span[1]]
        for t_span in merge_overlapped_spans(a_numbers)
        if t_span[0] < t_span[1]]
    return a_numbers


cpdef list merge_overlapped_spans(list a_spans):
    ''' take a list of spans/positions, sort them in order, then merge spans with any overlap
        return list of unique and distinct spans
        Arguments:
            a_spans: {list} of [(start_pos1, stop_pos1), (start_pos2, stop_pos2)...]
        Returns:
            {list} of non-overlapped [(start_pos1, stop_pos1), (start_pos2, stop_pos2)...]
    '''
    cdef list a_distinct = []
    cdef tuple t_span
    if not a_spans: return a_spans
    for t_span in sorted(a_spans, key=itemgetter(0)):
        if not a_distinct:
            a_distinct.append(t_span)
        elif a_distinct[-1][1] <= t_span[0]:
            a_distinct.append(t_span)
        elif t_span[1] > a_distinct[-1][1]:
            a_distinct[-1] = (a_distinct[-1][0], t_span[1])
    return a_distinct


cpdef int get_char_values(str s_chars):
    cdef str s_char
    return sum([e_CHAR_IMPORTANCE.get(s_char, 20) for s_char in s_chars])


cpdef list character_ngrams_by_len(list a_chars, int i_max_size=6, int i_min_size=1):
    ''' Returns a list of lists of ngrams from longest to shortest with sub-lists from first to last.'''
    cdef list a_all_ngrams
    cdef int i_size, i

    a_all_ngrams = []
    for i_size in reversed(range(i_min_size, min(i_max_size + 1, len(a_chars) + 1))):
        z_ngrams = zip(*[a_chars[i:] for i in range(i_size)])
        a_all_ngrams.append([''.join(x) for x in z_ngrams])
    return a_all_ngrams


cpdef list character_ngrams(list a_chars, int i_max_size=6, int i_min_size=1):
    ''' Returns a list of ngrams strings from longest to shortest from first to last for each size.'''
    cdef list a_all_ngrams
    cdef int i_size, i

    a_all_ngrams = []
    for i_size in reversed(range(i_min_size, min(i_max_size + 1, len(a_chars) + 1))):
        z_ngrams = zip(*[a_chars[i:] for i in range(i_size)])
        a_all_ngrams.extend([''.join(x) for x in z_ngrams])
    return a_all_ngrams


cpdef (int, int, int) count_important_edits(tn_edit):
    ''' Count the important, misc, and unimportant edits
        Arguments:
            tn_edit: {namedtuple} from FwEdditClassifier.edits()
        Returns
            {tuple} of {int} edit counts: (i_important, i_other, i_unimportant)
    '''
    cdef int i_important, i_other, i_unimportant
    cdef str s_char, s_char1, s_char2

    i_important, i_other, i_unimportant = 0, 0, 0
    for s_char in tn_edit.added:
        if s_char in c_IMPORTANT:
            i_important += 1
        elif s_char in c_UNIMPORTANT:
            i_unimportant += 1
        else:
            i_other += 1

    for s_char in tn_edit.removed:
        if s_char in c_IMPORTANT:
            i_important += 1
        elif s_char in c_UNIMPORTANT:
            i_unimportant += 1
        else:
            i_other += 1

    for s_char1, s_char2 in tn_edit.changed:
        if s_char1 in c_IMPORTANT_CHANGE or s_char2 in c_IMPORTANT_CHANGE:
            i_important += 1
        elif s_char1 in c_UNIMPORTANT and s_char2 in c_UNIMPORTANT:
            i_unimportant += 1
        else:
            i_other += 1

    for s_char1, s_char2 in tn_edit.transposed:
        i_other += 1

    return i_important, i_other, i_unimportant


################ FROM PyPhonetics ######################


def hamming_distance(word1, word2):
    """
    Computes the Hamming distance.

    [Reference]: https://en.wikipedia.org/wiki/Hamming_distance
    [Article]: Hamming, Richard W. (1950), "Error detecting and error correcting codes",
        Bell System Technical Journal 29 (2): 147–160
    """
    from operator import ne
    if len(word1) != len(word2):
        raise Exception(
            'The words need to be of the same length! "{}" ({}), "{}" ({})'.format(word1, len(word1), word2, len(word2)))

    return sum(map(ne, word1, word2))


def levenshtein_distance(word1, word2):
    """
    Computes the Levenshtein distance.

    [Reference]: https://en.wikipedia.org/wiki/Levenshtein_distance
    [Article]: Levenshtein, Vladimir I. (February 1966). "Binary codes capable of correcting deletions,
        insertions,and reversals". Soviet Physics Doklady 10 (8): 707–710.
    [Implementation]: https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Python
    """
    if len(word1) < len(word2):
        return levenshtein_distance(word2, word1)

    if len(word2) == 0:
        return len(word1)

    previous_row = list(range(len(word2) + 1))

    for i, char1 in enumerate(word1):
        current_row = [i + 1]

        for j, char2 in enumerate(word2):
            insertions = previous_row[j + 1] + 1
            deletions = current_row[j] + 1
            substitutions = previous_row[j] + (char1 != char2)

            current_row.append(min(insertions, deletions, substitutions))

        previous_row = current_row
    return previous_row[-1]


cdef class Metaphone:
    """ The metaphone algorithm.
        [Reference]: https://en.wikipedia.org/wiki/Metaphone
        [Author]: Lawrence Philips, 1990 """
    cdef list rules
    cdef dict distances
    def __init__(self):
        self.distances = {
            'levenshtein': levenshtein_distance,
            'hamming': hamming_distance,}

        self.rules = [
            (r'[^a-z]', r''),
            (r'([bcdfhjklmnpqrstvwxyz])\1+', r'\1'),
            (r'^ae', r'e'),
            (r'^[gkp]n', r'n'),
            (r'^wr', r'r'),
            (r'^x', r's'),
            (r'^wh', r'w'),
            (r'mb$', r'm'),
            (r'(?!^)sch', r'sk'),
            (r'[aeiou]nght', 'ngth'),
            #(r'th', r'0'),
            (r't?ch|sh', r'x'),
            (r'c(?=ia)', r'x'),
            (r'[st](?=i[ao])', r'x'),
            (r's?c(?=[iey])', r's'),
            (r'([cq]k*|(?<![sn])k(\b|$))', r'kk'),
            (r'dg(?=[iey])', r'j'),
            (r'd', r't'),
            (r'g(?=h[^aeiou])', r''),
            (r'gn(ed)?', r'n'),
            (r'([^g]|^)g(?=[iey])', r'\1j'),
            (r'g+', r'k'),
            (r'ph', r'f'),
            (r'([aeiou])h(?=\b|[^aeiou])', r'\1'),
            (r'[wy](?![aeiou])', r''),
            (r'z', r's'),
            (r'v', r'f'),
            (r'(?!^)[aeiou]+', r'')
        ]
        self.rules = [
            (re.compile(s_pattern), s_substitute)
            for s_pattern, s_substitute in self.rules
        ]
    
    cpdef str phonetics(self, str word):
        cdef str code, s_sub
        
        code = strip_unicode_accents(word).lower()
        
        for re_rule, s_sub in self.rules:
            code = re_rule.sub(s_sub, code)
        return code
    
    cpdef bint sounds_like(self, str word1, str word2):
        """Compare the phonetic representations of 2 words, and return a boolean value."""
        return self.phonetics(word1) == self.phonetics(word2)
    
    cpdef int distance(self, str word1, str word2, str metric='levenshtein'):
        """Get the similarity of the words, using the supported distance metrics."""
        if metric in self.distances:
            distance_func = self.distances[metric]
            return distance_func(self.phonetics(word1), self.phonetics(word2))
        else:
            raise Exception(
                'Distance metric "{}" not supported! Choose from levenshtein, hamming.'.format())
# end Metaphone class


cpdef float jaccard(x, y):
    """ returns the jaccard similarity between two lists """
    if isinstance(x, (list, tuple, dict)):
        x = set(x)
    assert isinstance(x, set)
    if isinstance(y, (list, tuple, dict)):
        y = set(y)
    assert isinstance(y, set)
        
    return jaccard_similarity(x, y)


cdef float jaccard_similarity(set x, set y):
    cdef int i_intersect = len(x.intersection(y))
    cdef float d_union = float(len(x.union(y)))
    if not d_union: return 0.0
    return i_intersect / d_union


cpdef float jaccard_text_ngrams(str s_text1, str s_text2, int i_max_size=6, int i_min_size=1):
    cdef list a_ngrams1, a_ngrams2, a_set1, a_set2
    cdef float d_total_score, d_total_possible, d_similarity, d_weight, d_score
    cdef int idx
    
    if not s_text1 or not s_text2: return 0.0
        
    a_ngrams1 = list(reversed(character_ngrams_by_len(list(s_text1), i_max_size=i_max_size, i_min_size=i_min_size)))
    a_ngrams2 = list(reversed(character_ngrams_by_len(list(s_text2), i_max_size=i_max_size, i_min_size=i_min_size)))
    
    #print(a_ngrams1, a_ngrams2)
    i_max_len = max(len(a_ngrams1[-1][0]), len(a_ngrams2[-1][0]))
    #print('max_len=', i_max_len)
    
    d_total_score = 0
    d_total_possible = 0
    for idx, (a_set1, a_set2) in enumerate(zip(a_ngrams1, a_ngrams2)): #reversed(range(i_min_size, i_max_len + 1)):
        d_similarity = jaccard_similarity(set(a_set1), set(a_set2))
        d_weight = (idx + 1) ** 2
        d_score = d_similarity * d_weight
        
        #print(idx + 1, d_similarity * d_weight)
        d_total_score += d_score
        d_total_possible += d_weight
    
    return d_total_score / d_total_possible


