﻿#cimport numpy as np
import numpy as np

import math, re
from itertools import groupby
from collections import Counter, namedtuple
from string import punctuation


__all__ = [
    "edits", "distance", "clear_edit_punctuation",
    "tn_EDITS", ]

cpdef str s_VOW = 'aeiouyAEIOUY'
cpdef str s_CONS = 'qwrtypsdfghjklzxcvbnmQWRTYPSDFGJJKLZXCVBNM'
cdef set c_TENSES = {"e", "ed", "es", "ing"}

cdef dict e_KEYS = {
    'Q': 'qASW!@12', 'q': 'Aasw12',
    'W': 'wQASDE@#23', 'w': 'Wqasde23',
    'E': 'eWSDFR#$34', 'e': 'Ewsdfr34',
    'R': 'rEDFGT$%45', 'r': 'Redfgt45',
    'T': 'tRFGHY%^56', 't': 'Trfghy56',
    'Y': 'yTGHJU^&67', 'y': 'Ytghju67',
    'U': 'uYHJKI&*78', 'u': 'Uyhjki78',
    'I': 'iUJKLO*(89', 'i': 'Iujklo89',
    'O': 'oIKLP()90', 'o': 'Oikl;p90',
    'P': "pOL)_[';:{0", 'p': "Pol;'[0-",
    'A': 'aZSWQ', 'a': 'Aqwsz',
    'S': 'sWAZXDE', 's': 'Swazxde',
    'D': 'dESXCFR', 'd': 'Desxcfr',
    'F': 'fRDCVGT', 'f': 'Frdcvgt',
    'G': 'gTFVBHY', 'g': 'Gtfvbhy',
    'H': 'hYGBNJU', 'h': 'Hygbnju',
    'J': 'jUHNMKI', 'j': 'Juhnmki',
    'K': 'kIJM<,LO', 'k': 'Kijm,lo',
    'L': 'lOK,.;P:><', 'l': 'Lok,.;p',
    'Z': 'zASX', 'z': 'Zasx',
    'X': 'xZSDC', 'x': 'Xzsdc',
    'C': 'cXDFV', 'c': 'Cxdfv',
    'V': 'vCFGB', 'v': 'Vcfgb',
    'B': 'bVGHN', 'b': 'Bvghn',
    'N': 'nBHJM', 'n': 'Nbhjm',
    'M': 'mNJK,<', 'm': 'Mnjk,'
}

cdef set c_adjacent
for s_letter, s_adjacent in e_KEYS.items():
    c_adjacent = set(s_adjacent)
    e_KEYS[s_letter] = c_adjacent

a_SOUND = ['eahjk', 'bsdgptvz', 'flmnsxz', 'iy', 'quw']
a_SOUND.extend([s_char.upper() for s_char in a_SOUND])
a_CON_PAIRS = ['ck', 'ch', 'sh', 'sch', 'th', 'gh', 'ph']

e_PUNCT = set(punctuation)
e_PUNCT.add(" ")
tn_EDITS = namedtuple("edits", ["added", "removed", "changed", "transposed"])
tn_EDITS_WITH_DUPLICATION = namedtuple("edits", ["added", "removed", "changed", "transposed", "duplicated", "deduplicated"])


cpdef edits(str s_word1, str s_word2, bint duplication=False, bint ignore_punctuation=False, bint ignore_case=True):
    ''' Returns which changes are required to transfrom s_word1 into s_word2 '''
    cdef str s_x, s_y
    cdef int i_rows, i_cols, i_x, i_y, i, j, i_DistHor, i_DistVer, i_DistDiag
    cdef list a_dist, a_align
    cdef list a_added, a_removed, a_changed, a_transposed, a_duplicated, a_deduplicated
    
    if ignore_case:
        s_x, s_y = s_word1.lower(), s_word2.lower()
    else:
        s_x, s_y = s_word1, s_word2
    i_rows, i_cols = len(s_x), len(s_y)
    
    if not s_word1 and not s_word2:
        if duplication:
            return tn_EDITS_WITH_DUPLICATION([], [], [], [], [], [])
        else:
            return tn_EDITS([], [], [], [])
    if not s_word1:
        if duplication:
            return tn_EDITS_WITH_DUPLICATION([], list(s_word2), [], [], [], [])
        else:
            return tn_EDITS([], list(s_word2), [], [])
    if not s_word2:
        if duplication:
            return tn_EDITS_WITH_DUPLICATION(list(s_word1), [], [], [], [], [])
        else:
            return tn_EDITS(list(s_word1), [], [], [])
    
    
    # Creating matrixes a_dist for calulating distanse and a_align to traceback changes
    a_dist = [[0] * (i_cols + 1) for i_x in range(i_rows + 1)]
    a_align = [[0] * (i_cols + 1) for i_x in range(i_rows + 1)]
    a_added, a_removed, a_changed, a_transposed = [], [], [], []
    a_duplicated, a_deduplicated = [], []
    
    # Initializing first column and first row of a_dist
    for i_x in range(1, i_rows + 1): a_dist[i_x][0] = i_x
    for i_y in range(1, i_cols + 1): a_dist[0][i_y] = i_y

    # Filling a_dist with minimal value for distanse from horizontal, vertical, diagonal
    for i_x in range(1, i_rows + 1):
        for i_y in range(1, i_cols + 1):
            i_DistHor = a_dist[i_x][i_y - 1] + 1
            i_DistVer = a_dist[i_x - 1][i_y] + 1
            
            if s_x[i_x - 1] == s_y[i_y - 1]:
                i_DistDiag = a_dist[i_x - 1][i_y - 1]
            else:
                i_DistDiag = a_dist[i_x - 1][i_y - 1] + 1

            a_dist[i_x][i_y] = min(i_DistHor, i_DistVer, i_DistDiag)

            if a_dist[i_x][i_y] == i_DistDiag:
                a_align[i_x][i_y] = 2
            if a_dist[i_x][i_y] == i_DistHor:
                a_align[i_x][i_y] = 1
            if a_dist[i_x][i_y] == i_DistVer:
                a_align[i_x][i_y] = 0

    # Trace back a_align to identify changes that are to be made to transform s_x to s_y
    i, j = len(a_align) - 1, len(a_align[0]) - 1

    while (i > 0 and j > 0):
        i_x, i_y = i - 1, j - 1
        
        if (
            i > 1 and j > 1 and s_y[i_y] != s_x[i_x] and 
            s_x[i_x] == s_y[j - 2] and s_x[i - 2] == s_y[i_y] and
            s_x[i_x] != s_x[i_x - 2] and s_y[i_y] != s_y[i_y - 2]
        ):
            a_transposed.append((s_x[i_x], s_y[i_y]))
            i, j = i - 2, j - 2
        
        elif a_align[i][j] == 0:
            i = i_x
            if duplication and s_x[i] == s_x[i-1]:
                a_duplicated.append(s_x[i])
            else:
                a_added.append(s_x[i])

        elif a_align[i][j] == 1:
            j = i_y
            if duplication and s_y[j] == s_y[j-1]:
                a_deduplicated.append(s_y[j])
            else:
                a_removed.append(s_y[j])
            
        elif a_align[i][j] == 2:

            i, j  = i_x, i_y
            if (s_y[j] != s_x[i]):
                a_changed.append((s_x[i], s_y[j]))
                
        else:
            i, j  = i_x, i_y
        
    for i_x in reversed(range(1, i + 1)): a_added.append(s_x[i_x - 1])
    for i_y in reversed(range(1, j + 1)): a_removed.append(s_y[i_y - 1])
    
    a_added = a_added[::-1]
    a_removed = a_removed[::-1]
    a_changed = a_changed[::-1]
    a_transposed = a_transposed[::-1]
    a_duplicated = a_duplicated[::-1]
    a_deduplicated = a_deduplicated[::-1]

    if ignore_punctuation:
        clear_edit_punctuation(a_added, a_removed, a_changed, a_transposed, a_duplicated, a_deduplicated)
    
    if duplication:
        return tn_EDITS_WITH_DUPLICATION(a_added, a_removed, a_changed, a_transposed, a_duplicated, a_deduplicated)
    else:
        return tn_EDITS(a_added, a_removed, a_changed, a_transposed)


cpdef int distance(str s_1, str s_2, bint b_case_insensitive=False):
    ''' returns: character damerau-levenshtein distance '''
    cdef int i_len1, i_len2, x, y, i_x, i_y, cost
    cdef str s_x, s_y
    
    if s_1 == s_2: return 0
    i_len1, i_len2 = len(s_1), len(s_2)
    if i_len1 == 0: return i_len2
    if i_len2 == 0: return i_len1
    
    np_dist = np.zeros([i_len1 + 1, i_len2 + 1], dtype=int)
    
    if b_case_insensitive:
        s_1, s_2 = s_1.lower(), s_2.lower()
      
    np_dist[:, 0] = np.array(range(i_len1 + 1), dtype=int)
    np_dist[0, :] = np.array(range(i_len2 + 1), dtype=int)
    
    for i_x, s_x in enumerate(s_1):
        x = i_x + 1
        for i_y, s_y in enumerate(s_2):
            if s_x == s_y:
                cost = 0
            else:
                cost = 1
                
            y = i_y + 1
            np_dist[x, y] = min(
                np_dist[x - 1, y] + 1,  # deletion
                np_dist[x, y - 1] + 1,  # insertion
                np_dist[x - 1, y - 1] + cost  #substitution
            )
            # transposition
            if i_x and i_y and s_x == s_2[i_y - 1] and s_1[i_x - 1] == s_y:
                np_dist[(x, y)] = min (np_dist[(x, y)], np_dist[x-2, y-2] + cost)
                
    return np_dist[-1, -1]


cpdef clear_edit_punctuation(list a_added, list a_removed, list a_changed,list a_transposed,
                             list a_duplicated=[], list a_deduplicated=[]):
    ''' Remove punctuation characters from edit lists.
        This happens in place, so no need to return it. '''
    cdef str s_char
    cdef tuple t_change
    cdef bint b_char1_punct, b_char2_punct
    
    for s_char in reversed(a_removed):
        if s_char in e_PUNCT:
            a_removed.remove(s_char)
    
    for s_char in reversed(a_added):
        if s_char in e_PUNCT:
            a_added.remove(s_char)
    
    for t_change in reversed(a_changed):
        b_char1_punct = t_change[0] in e_PUNCT
        b_char2_punct = t_change[1] in e_PUNCT
        
        if not b_char1_punct and not b_char2_punct:
            pass
        
        elif b_char1_punct and not b_char2_punct:
            a_removed.append(t_change[1])
            a_changed.remove(t_change)
    
        elif not b_char1_punct and b_char2_punct:
            a_added.append(t_change[0])
            a_changed.remove(t_change)
            
        elif b_char1_punct and b_char2_punct:
            a_changed.remove(t_change)
    
    for t_change in reversed(a_transposed):
        b_char1_punct = t_change[0] in e_PUNCT
        b_char2_punct = t_change[1] in e_PUNCT

        if b_char1_punct or b_char2_punct:
            a_transposed.remove(t_change)
    
    for s_char in reversed(a_duplicated):
        if s_char in e_PUNCT:
            a_duplicated.remove(s_char)
    
    for s_char in reversed(a_deduplicated):
        if s_char in e_PUNCT:
            a_deduplicated.remove(s_char)
