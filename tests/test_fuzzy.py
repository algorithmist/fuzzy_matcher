﻿'''
Test cases for FwFuzzy/Fuzzy
'''
from __future__ import absolute_import
from fuzzy_matcher.fuzzy_texts import FuzzyTexts


class TestFuzzyTexts():
    ''' Test library for medium length string fuzzy match '''
    def test_empty(self):
        o_fuzzy = FuzzyTexts([])
        assert not o_fuzzy.find('anything')
        assert not o_fuzzy.search('anything')
        
    def test_exact(self):
        o_fuzzy = FuzzyTexts(["one", "two", "three", "four", "five", "sixseven"], i_min_ngram_chars=3)
        result = o_fuzzy.find('one')
        assert result
        assert result[0] == "one"
        assert result[1] == 1.0
        
    def test_change(self):
        o_fuzzy = FuzzyTexts(["one", "two", "three", "four", "five", "sixseven"], i_min_ngram_chars=3)
        result = o_fuzzy.find('threw')
        assert result
        assert result[0] == "three"
        assert result[1] >= 0.5
    
    def test_phonetic(self):
        o_fuzzy = FuzzyTexts(["one", "two", "three", "four", "five", "sixseven"], i_min_ngram_chars=3)
        result = o_fuzzy.find('phive')
        assert result
        assert result[0] == "five"
        assert result[1] >= 0.4
        
    def test_partial(self):
        o_fuzzy = FuzzyTexts(["one", "two", "three", "four", "five", "sixseven"], i_min_ngram_chars=3)
        result = o_fuzzy.find('six')
        assert result
        assert result[0] == "sixseven"
        assert result[1] >= 0.3
        
    #def test_acronym(self):
    #    o_fuzzy = FuzzyTexts(["one", "two", "three", "four", "five", "sixseven"], i_min_ngram_chars=3)
    #    assert o_fuzzy.find('T.W.O.') ==  "two"
    #    result = o_fuzzy.search('T.W.O.')
    #    assert len(result) == 1
    #    assert result[0][0] == "two"
    #    assert result[0][1] >= 0.9
    
    #def test_transposition(self):
    #    o_fuzzy = FuzzyTexts(["one", "two", "three", "four", "five", "sixseven"], i_min_ngram_chars=3)
    #    assert o_fuzzy.find('fuor') ==  "four"
    #    result = o_fuzzy.search('fuor')
    #    assert len(result) == 1
    #    assert result[0][0] == "four"
    #    assert result[0][1] >= 0.5 
