# ### Fuzzy Matching of words ### #

For finding the closest match for a phrase from a list of phrases (like brands or product titles). This uses character ngrams to search for the closest potential matches, then does a more detailed comparison to find the best match(es). It is much faster than a brute force levenshtein or ngram-jaccard search when there is a large list of phrases to match each search against.

It isn't intended for extremely short strings like individual words that would be needed for a spell checker, nor extremely long strings to find closest paragraphs for a plagarism detector.

Finding the closest candidates is insensitive word order in the phrase, but the final comparison will take into account order when selecting best match(es).

Example usage:

```
#!python

from fuzzy_match.fuzzy_texts import FuzzyTexts

a_phrases = [
    "This is the first example phrase",
    "Phrase two in the list",
    "Yet another short text phrase", ...]

o_fuzzy = FuzzyTexts(a_phrases)

# list of the best guess in descending order of similarity and with the score from the detailed comparison
a_matches = o_fuzzy.search('short txt phrase')

# single closest string matched with it's similarity store
s_match = o_fuzzy.find('phrase two')

```
