import os, sys

import setuptools
from setuptools.command.install import install

module_path = os.path.join(os.path.dirname(__file__), 'fuzzy_matcher/__init__.py')
version_line = [line for line in open(module_path)
                if line.startswith('__version__')][0]

__version__ = version_line.split('__version__ = ')[-1][1:][:-2]
            
class custom_install(install):
    def run(self):
        self.install_bitbucket_dependencies()
        print("This is a custom installation")
        install.run(self)
        
    def install_bitbucket_dependencies(self):
        if sys.version_info.major == 2:
            #os.system("pip install git+https://bitbucket.org/")
            pass
        elif sys.version_info.major == 3:
            #os.system("pip3 install git+https://bitbucket.org/")
            pass

setuptools.setup(
    name="FuzzyMatcher",
    version=__version__,
    url="https://bitbucket.org/algorithmist/fuzzy_matcher",

    author="Tim Gilbert",

    description="Fuzzy text matching class",
    long_description=open('README.md').read(),

    packages = setuptools.find_packages(),
    package_data={'': ["*.pyx", "*.py", "*.txt"]},
    py_modules=['fuzzy_matcher'],
    zip_safe=False,
    platforms='any',

    install_requires=[
        "cython",
    ],
    cmdclass={'install': custom_install}
)


